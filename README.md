# Blinker Demo Application
**Version: 1.000**<br/>
**Release Date: 2024/01/08**

This application provides a simple temperature sensor blinking green when temperature is above 13C and red when below.  Meant for
show demonstrations.

## Key Features  
All location features are disabled unless activated.

Key features:
1. Blink Green when temperature about 13 C
2. Blink Red when temperature below 13 C
3. Reports when temperature falls below or goes above 13 C.
4. Samples temperature every 10 seconds.
5. Activate/Deactivate blinker by clicking three times.
6. Activate/Deactivate location reporting by clicking 5 times.
7. Battery status events and report
8. Health Check two once per day.
9. All functions disabled when device is reset.

# User Interface 
This section describes the essential functions of the CT1000 user interface as defined
by this application.   

## Button Commands  

The following tables summarize the button activated commands organized into primary operational and additional commands.  
### Operational Commands 
for ordinary operational use.
| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Activate/Deactivate Blinker**     | three (3) quick presses | Button #1 | Activates / deactivates blinker function.  Typically activated for show and deactivated after that. |  
| **Activate/Deactivate Location**     | five (5) quick presses  | Button #1 | Activates / deactivates location function. |  
| **Battery Status**                  | one (1) quick press     | Button #2 | Indicates battery level <br> [*See Battery Indicator*](#battery-indicator) 
| **Network Reset**                   | hold > 15<br>seconds              | any button or both | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds              | any button or both | Resets the network and configuration to factory defaults for the application.     |
 
## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads 

| **Description**                |                **Indication**               |   **LEDS**  |
|--------------------------------|:-------------------------------------------:|:-----------:|
| **> 13 C Temperature Indication** |  Green Blink once per second |    LED #1     |
| **< 13 C Temperature Indication** |  Red Blink once per second   |    LED #1     |
| **Location Mode activating/deactivating**   |  Green/Red blinks <br>three (3) times quickly  |    LED #2    |
| **Device Reset**               |       Green blink <br>three (3) times       |    both     |
| **Battery Charging**           | Orange slow blink every<br>ten (10) seconds |    LED #2   |
| **Battery Fully Charged**      |  Green slow blink every<br>ten (10) seconds |    LED #2   |

## Battery Indicator 

Activating the battery status commend will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description|
|----------|-------------|------------|
|  1 red   |   < 5%      | Battery level is critical,  charge immediately. |
|  1 green |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 green |  30% - 50%  | Battery is about 40% charged. |
|  3 green |  50% - 70%  | Battery is about 60% charged. |
|  4 green |  70% - 90%  | Battery  is about 80% charged. |
|  5 green |  > 90%      | Battery is fully charged. |

The tag will report battery status when connect or disconnect from charging. The battery level and temperature are monitored every 10 minutes. The tag will also report battery status when its level change more than 5%. The tag will go to low power mode and stop automatic location reporting when battery level is below 20%. The tag will resume automatic location reporting after battery 
level return to above 25%. 

---
*Copyright 2024, Codepoint Technologies, Inc.* <br>
*All Rights Reserved*
